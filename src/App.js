import { useContext } from 'react';
import BookList from './components/Book/BookList.js';
import Header from './components/Header.js';
import PurchaseForm from './components/purchase/PurchaseForm.js';
import Store from './components/store/Store.js';
import { AppContext } from './context/AppContext.js';
import { Switch, Route } from 'react-router-dom';
import BookForm from './components/Book/BookForm.js';

function App() {

  const { books, favorites, purchaseList: purchases } = useContext(AppContext);

  const favoritebooks = books.filter((book) => favorites.indexOf(book.id) !== -1);

  return (
    <div className="App w-full h-screen bg-gray-100 overflow-auto">
      <Header />
      <main>
        <Switch>
          <Route path="/" exact>
            <BookList title="Book List" books={books} />
          </Route>
          <Route path="/favorites">
            <BookList title="Favorites" books={favoritebooks} />
          </Route>
          <Route path="/purchases">
            <BookList title="Purchase List" books={purchases} purchaselist />
          </Route>
          <Route path="/store">
            <Store />
          </Route>
          <Route path="/newbook">
            <BookForm />
          </Route>
          <Route path="/editbook/:bookID">
            <BookForm />
          </Route>
          <Route path="/purchasebook/:bookID">
            <PurchaseForm />
          </Route>
        </Switch>
      </main>
    </div>
  );
}

export default App;
