import { createContext, useState } from "react";
import { books as booksData } from '../data';

export const AppContext = createContext({});

export const AppProvider = (props) => {

  const [books, setBooks] = useState(booksData);
  const [favorites, setFavorites] = useState([]);
  const [purchaseList, setPurchaseList] = useState([]);


  const handlePurchaseSubmit = (purchase) => {

    const now = new Date();
    const [book] = books.filter(book => book.id === +purchase.bookID);

    const purchasedBook = {
      ...book,
      purchaseDate: now.toDateString(),
      purchasedBy: purchase.purchasedBy
    }

    setPurchaseList((purchases) => [...purchases, purchasedBook]);
  }


  // check if the book is in the wish list or not
  const isFavorite = (bookID) => {
    return favorites.indexOf(bookID) !== -1;
  }

  const addOrUpdateBook = (book) => {

    if (book.id === -1) {
      book.id = +(books.slice().sort((a, b) => +b.id - +a.id)[0]?.id || 0) + 1;
      setBooks((books) => [...books, book]);
    } else {
      setBooks((books) => books.map(b => b.id === +book.id ? book : b))
    }

  }


  const deleteBook = (bookID) => {
    setBooks((books) => books.filter(book => book.id !== +bookID));
    toggleFavorite(bookID);
  }

  // add or remove the book from wish list
  const toggleFavorite = (bookID) => {

    if (!isFavorite(bookID)) {
      setFavorites((favorites) => [...favorites, bookID]);
    }
    else {
      setFavorites((favorites) => favorites.filter(id => id !== bookID));
    }
  }


  const context = {
    books,
    favorites,
    purchaseList,
    isFavorite,
    toggleFavorite,
    handlePurchaseSubmit,
    addOrUpdateBook,
    deleteBook
  }

  return <AppContext.Provider value={context} {...props} />
}