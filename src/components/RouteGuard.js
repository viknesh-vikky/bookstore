import React, { useEffect, useState } from 'react';
import { Prompt, useHistory } from 'react-router-dom';
import ConfirmDialog from './ConfirmDialog';

const RouteGuard = ({
  when,
  titleText = "Close without saving?",
  contentText = "You have unsaved changes. Are you sure you want to leave this page without saving?",
  cancelButtonText = "CANCEL",
  confirmButtonText = "CONFIRM",
  shouldBlockNavigation = () => true
}) => {

  const [modalVisible, setModalVisible] = useState(false);
  const [lastLocation, setLastLocation] = useState(null);
  const [confirmedNavigation, setConfirmedNavigation] = useState(false);
  const history = useHistory();

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleBlockedNavigation = (nextLocation) => {
    if (!confirmedNavigation && shouldBlockNavigation(nextLocation)) {
      setModalVisible(true);
      setLastLocation(nextLocation);
      return false;
    }
    return true;
  };

  const handleConfirmNavigationClick = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
  };

  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      history.push(lastLocation.pathname);
    }
  }, [confirmedNavigation, lastLocation, history]);

  return (
    <>
      <Prompt when={when} message={handleBlockedNavigation} />
      <ConfirmDialog
        open={modalVisible}
        titleText={titleText}
        contentText={contentText}
        cancelButtonText={cancelButtonText}
        confirmButtonText={confirmButtonText}
        onCancel={closeModal}
        onConfirm={handleConfirmNavigationClick}
      />
    </>
  );
};

export default RouteGuard;