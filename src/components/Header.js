import React from 'react'
import { Link } from 'react-router-dom'

export default function Header() {
  return (
    <>
      <header className="p-5 bg-blue-600 text-gray-50 flex items-center justify-between shadow">
        <Link to="/">
          <button className="title text-2xl font-semibold">Books</button>
        </Link>
        <div className="flex">
          <Link to="/favorites">
            <button className="mx-3 py-2 flex item-center border-b-2 border-transparent hover:border-gray-50" title="favorites">
              <i className="material-icons-round text-md mr-2">favorite</i>
              Favorites
            </button>
          </Link>
          <Link to="/purchases">
            <button className="mx-3 py-2 flex item-center border-b-2 border-transparent hover:border-gray-50" title="Purchase history">
              <i className="material-icons-round text-md mr-2">receipt_long</i>
              Purchases
            </button>
          </Link>
          <Link to="/store">
            <button className="mx-3 py-2 flex item-center border-b-2 border-transparent hover:border-gray-50" title="Book Store">
              <i className="material-icons-round text-md mr-2">inventory</i>
              Store
            </button>
          </Link>
        </div>
      </header>
    </>
  )
}
