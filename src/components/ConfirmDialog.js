import React from 'react'

export default function ConfirmDialog({
  open = false,
  titleText = "Confirm",
  contentText = "Are you sure?",
  cancelButtonText = "CANCEL",
  confirmButtonText = "CONFIRM",
  onCancel,
  onConfirm }) {

  if (!open) return null;

  return (
    <>
      <div className="w-full h-screen fixed top-0 left-0 z-50 bg-gray-900 bg-opacity-50 flex items-start justify-center p-5">
        <div className="flex-grow max-w-sm bg-gray-50 text-gray-800 p-5 rounded">
          <p className="font-semibold text-2xl">{titleText}</p>
          <p className="text-sm tracking-wide text-gray-700 my-5">{contentText}</p>
          <div className="flex items-center justify-end w-full mt-10">
            <button className="px-3 py-2 rounded mx-3 text-sm font-semibold text-gray-600 bg-gray-200 hover:bg-gray-300 tracking-wider" onClick={onCancel}>{cancelButtonText}</button>
            <button className="px-3 py-2 rounded text-sm font-semibold bg-blue-600 hover:bg-blue-700 text-gray-50 tracking-wider" onClick={onConfirm}>{confirmButtonText}</button>
          </div>
        </div>
      </div>
    </>
  )
}
