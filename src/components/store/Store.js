import React from 'react'
import { Link } from 'react-router-dom'
import BookTable from './BookTable'

export default function Store() {

  return (
    <>
      <section className="p-5">
        <div className="flex items-center justify-between">
          <span className="font-bold text-2xl my-4 border-l-4 border-blue-600 px-2">Store</span>
          <div className="store-actions">
            <Link to="/newbook">
              <button className="flex items-center px-4 py-2 rounded bg-blue-600 text-gray-50 hover:bg-blue-700">
                <i className="material-icons-round mr-3">post_add</i>
                <span>New Book</span>
              </button>
            </Link>
          </div>
        </div>
        <BookTable />
      </section>
    </>
  )
}
