import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AppContext } from '../../context/AppContext'
import ConfirmDialog from '../ConfirmDialog';

export default function BookTable() {

  const { books, deleteBook, isFavorite } = useContext(AppContext);
  const [showWarning, setShowWarning] = useState(false);
  const [deletingBook, setDeletingBook] = useState(null);

  return (
    <>
      <div className="flex flex-col mt-5">
        <div className="overflow-x-auto">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Author
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Ratings
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Price
                    </th>
                    <th scope="col"
                      className="px-1 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Edit
                    </th>
                    <th scope="col"
                      className="px-1 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Delete
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {books.map((book) => (
                    <tr key={book.id}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="flex-shrink-0 h-10 w-10">
                            <img className="h-10 w-10 rounded-full" src={book.img} alt={book.name} />
                          </div>
                          <div className="ml-4">
                            <div className="text-sm font-medium text-gray-900">{book.name}</div>
                            <div className="text-xs my-1 text-gray-500">{book.pages} pages</div>
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-sm text-gray-900">{book.author}</div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="text-xs font-bold flex items-center my-2 px-2 py-0.5 text-gray-50 bg-green-600 rounded w-max">
                          {book.ratings}&nbsp;<i className="material-icons-round text-sm">star_rate</i>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-600 font-semibold">₹ {book.price}</td>
                      <td className="px-1 py-4 whitespace-nowrap text-sm font-medium">
                        <Link to={`/editbook/${book.id}`}>
                          <button title="Edit" className="material-icons-round text-gray-500">
                            edit
                          </button>
                        </Link>
                      </td>
                      <td className="px-1 py-4 whitespace-nowrap text-sm font-medium">
                        <button title="Delete" className="material-icons-round text-gray-500"
                          onClick={() => {
                          if (isFavorite(book.id)) {
                            setShowWarning(true);
                            setDeletingBook(book.id);
                          } else {
                            deleteBook(book.id);
                          }
                        }}>
                          delete
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <ConfirmDialog
        open={showWarning}
        titleText="Warning!"
        contentText="This book will be removed from favorite list. Are you sure want to delete?"
        confirmButtonText="DELETE"
        onConfirm={() => {
          deleteBook(deletingBook);
          setShowWarning(false);
        }}
        onCancel={()=> setShowWarning(false)}
      />
    </>
  )
}
