import React, { useContext, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router';
import { VALIDATIONS } from '../../constants/forms';
import { AppContext } from '../../context/AppContext';
import RouteGuard from '../RouteGuard';
import Book from './Book';


export default function BookForm() {

  const { books, addOrUpdateBook } = useContext(AppContext);

  const { bookID } = useParams();
  const history = useHistory();

  const book = books.filter(b => b.id === +bookID)[0] || null;

  const { register, handleSubmit, reset, getValues, watch, formState: { errors, isSubmitSuccessful, isDirty } } = useForm({
    mode: 'all',
    defaultValues: book || {
      id: -1,
      name: '',
      author: '',
      cover: '',
      pages: '',
      price: '',
      story: '',
      ratings: '',
      img: '/default_book.jpg'
    }
  });

  const [newBook, setNewBook] = useState(getValues());


  const formFields = [
    {
      name: 'name',
      type: 'text',
      label: 'Name of the Book',
      registerParams: {
        required: VALIDATIONS.REQUIRED
      }
    },
    {
      name: 'author',
      type: 'text',
      label: 'Author Name',
      registerParams: {
        required: VALIDATIONS.REQUIRED
      }
    },
    {
      name: 'cover',
      type: 'text',
      label: 'Type of Cover',
      registerParams: {
        required: VALIDATIONS.REQUIRED
      }
    },
    {
      name: 'pages',
      type: 'text',
      label: 'No. of Pages',
      registerParams: {
        required: VALIDATIONS.REQUIRED,
        pattern: {
          value: /[0-9]+/,
          message: 'Invalid Pages'
        }
      }
    },
    {
      name: 'price',
      type: 'text',
      label: 'Price',
      registerParams: {
        required: VALIDATIONS.REQUIRED,
        pattern: {
          value: /[0-9]+/,
          message: 'Invalid Price'
        }
      }
    },
    {
      name: 'ratings',
      type: 'text',
      label: 'Ratings',
      registerParams: {
        required: VALIDATIONS.REQUIRED,
        pattern: {
          value: /^[1-5]([.][0-9])?$/,
          message: 'Invalid rating. Enter a value between 1.0 and 5.0'
        }
      }
    },
    {
      name: 'img',
      type: 'text',
      label: 'Cover Image',
      placeholder: '/default_book.jpg',
      registerParams: {}
    }
  ];


  useEffect(() => {

    !isSubmitSuccessful && reset();

    if (isSubmitSuccessful) {
      book ? history.push('/store') : history.push('/');
    }

  }, [isSubmitSuccessful, reset, book, history]);

  useEffect(() => {
    const subscription = watch((value) => setNewBook(value));
    return () => subscription.unsubscribe();
  }, [watch]);



  return (
    <>
      <section className="p-5">
        <div className="flex items-center justify-between">
          <span className="font-bold text-2xl my-4 border-l-4 border-blue-600 px-2">
            {
              book ? 'Edit Book' : 'Add New Book'
            }
          </span>
        </div>
        <div className="flex flex-col md:flex-row items-start justify-center mt-5">
          <Book book={newBook} preview />
          <form className="rounded w-full max-w-lg mt-10 md:mt-0 md:ml-10 flex flex-col px-6" autoComplete="false" autoCorrect="false" onSubmit={handleSubmit(addOrUpdateBook)}>
            <input id="id" className="w-full border border-gray-300 rounded p-2 focus:border-blue-600"
              {...register('id')} type="number" hidden />

            {
              formFields.map((field) => (
                <div key={`${field.name}-${field.type}`} className="my-3 w-full">
                  <label htmlFor={field.name}>
                    <p className="font-semibold mb-2">
                      {field.label}
                      {field.registerParams?.required && <span className="text-red-600"> *</span>}
                    </p>
                  </label>
                  <input
                    id={field.name}
                    className="w-full border-2 border-gray-300 text-gray-800 rounded p-2 focus:border-blue-600"
                    type={field.type}
                    placeholder={field.placeholder}
                    {...register(field.name, field.registerParams)}
                  />
                  {
                    errors[field.name] &&
                    <p className="text-red-600 mt-1 text-sm tracking-wide">
                      {errors[field.name].message}
                    </p>
                  }
                </div>
              ))
            }

            <div className="my-3 w-full">
              <label htmlFor="story">
                <p className="font-semibold mb-2">
                  Story<span className="text-red-600"> *</span>
                </p>
              </label>
              <textarea id="story" rows={5} className="w-full border-2 border-gray-300 text-gray-800 h-auto rounded p-2 focus:border-blue-600 resize-y"
                {...register('story', {
                  required: VALIDATIONS.REQUIRED
                })}
              ></textarea>
              {
                errors.story &&
                <p className="text-red-600 mt-1 text-sm tracking-wide">
                  {errors.story.message}
                </p>
              }
            </div>
            <button className="px-4 py-2 bg-blue-600 rounded my-2 text-gray-50" type="submit">
              {
                book ? 'Update Book' : 'Add New Book'
              }
            </button>
          </form>
        </div>
      </section>
      <RouteGuard when={isDirty && !isSubmitSuccessful} />
    </>
  )
}
