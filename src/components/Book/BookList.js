import React, { memo } from 'react';
import Book from './Book';
import { v4 as uuidv4 } from 'uuid';

const BookList = ({ title, books, purchaselist = false }) => {

  return (
    <>
      <section className="p-5 w-full">
        <div className="font-bold text-2xl my-4 border-l-4 border-blue-600 px-2">{title}</div>
        {books.length === 0 ?
          (
            <p className="text-gray-400 text-center p-4">No Books in {title}...!</p>
          )
          : (
            <div className="flex flex-wrap items-center justify-start mx-auto mb-5">
              {
                books.map((book) => <Book key={uuidv4(`${book.id}-${book.name}`)} book={book} purchaselist={purchaselist} />)
              }
            </div>
          )
        }
        <hr />
      </section>
    </>
  )
}


export default memo(BookList);

