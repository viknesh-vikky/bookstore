import React, { memo, useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AppContext } from '../../context/AppContext';
import PurchaseDetails from '../purchase/PurchaseDetails';

const Book = ({ book, purchaselist = false, preview = false }) => {

  const [showDetail, setShowDetail] = useState(false);
  const { toggleFavorite, isFavorite } = useContext(AppContext);
  const { name, author, img, cover, price, ratings, story, pages } = book;

  return (
    <>
      {
        showDetail && <div className="book-placeholder flex-grow w-96 max-w-md"></div>
      }
      <div id="bookDetailsOverlay" className={`${showDetail ? 'fixed w-full h-screen top-0 left-0 bg-black bg-opacity-50 flex items-center justify-center z-40' : 'flex-grow w-96 max-w-md'}`}>
        <div className="book max-w-md shadow p-6 rounded-lg bg-gray-50 m-2 hover:shadow-xl relative">
          {
            showDetail &&
            <button className="material-icons-round absolute top-0 right-0 pr-2 pt-2 text-3xl text-gray-300 hover:text-gray-400" onClick={() => {
              setShowDetail(false);
            }}>
              clear
            </button>
          }
          <div className="w-full flex flex-grow items-start justify-start">
            <div>
              <img src={img} alt={name} className="w-32 h-36 m-auto" />
              <div className="text-xs text-gray-500 text-center mt-2">{pages} pages</div>
            </div>
            <div className="w-full h-full flex flex-col pl-6 justify-between">
              <h6 className="text-lg font-semibold text-blue-600 mb-2">{name}</h6>
              <h4 className="text-sm italic mb-5 text-gray-800">~by {author}</h4>
              <p className="text-sm text-gray-600">{cover}</p>
              <div className="text-xs font-bold flex items-center my-2 px-2 py-0.5 text-gray-50 bg-green-600 rounded w-max">
                {ratings}&nbsp;
                <i className="material-icons-round text-sm">star_rate</i>
              </div>
              <h6 className={`text-sm text-gray-800 mt-2 ${!showDetail ? 'max-line-2' : ''}`}>{story}</h6>
              {!showDetail && <button className="w-max text-sm text-blue-700 hover:underline" onClick={() => setShowDetail(true)}>view details</button>}
            </div>
          </div>

          {
            !purchaselist && (
              <div className="action w-full flex items-center justify-center text-gray-50 mt-5">
                {
                  !preview ?
                    <Link to={`/purchasebook/${book.id}`} className="buy px-4 py-2 flex-grow flex items-center justify-center bg-blue-600 rounded hover:bg-blue-700 font-semibold">
                      <button title="Purchase this book">
                        <span>Buy &nbsp;₹{price} only!</span>
                      </button>
                    </Link>
                    :
                    <button title="Purchase this book" className="buy px-4 py-2 flex-grow flex items-center justify-center bg-blue-600 rounded hover:bg-blue-700 font-semibold">
                      <span>Buy &nbsp;₹{price} only!</span>
                    </button>
                }
                <button title="Wishlist" className={`add_to_wishlist p-2 flex-shrink-0 ml-2 ${(!purchaselist && isFavorite(book.id)) ? 'text-pink-600' : 'text-gray-400 hover:text-pink-400'}`}
                  onClick={(e) => {
                    if (preview) return;
                    toggleFavorite(book.id);
                    e.stopPropagation();
                  }}
                >
                  <i className="material-icons-round text-2xl">favorite</i>
                </button>
              </div>
            )
          }

          {
            purchaselist && <PurchaseDetails book={book} />
          }
        </div>
      </div >
    </>
  )
}

export default memo(Book);