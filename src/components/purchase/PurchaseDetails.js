import React from 'react'

export default function PurchaseDetails({ book }) {
  return (
    <>
      <hr className="my-4 w-full" />
      <p className="text-gray-800 text-sm p-2 mb-4">Customer Details</p>
      <div className="w-full flex text-gray-800">
        <div className="flex-grow">
          <h6 className="font-semibold flex items-center"><i className="material-icons-round text-base mr-4">person</i> {book.purchasedBy.name}</h6>
          <h6 className="text-sm my-1 flex items-center"><i className="material-icons-round text-base mr-4">today</i> {book.purchaseDate}</h6>
          <h6 className="text-sm my-1 flex items-center"><i className="material-icons-round text-base mr-4">mail</i><span className="text-blue-600 underline">{book.purchasedBy.email}</span></h6>
          <h6 className="text-sm flex items-center"><i className="material-icons-round text-base mr-4">phone</i>{book.purchasedBy.contact}</h6>
        </div>
        <div className="flex-grow font-semibold text-gray-800 text-2xl flex items-center justify-center">
          ₹<b>{book.price}</b>
        </div>
      </div>
    </>
  )
}
