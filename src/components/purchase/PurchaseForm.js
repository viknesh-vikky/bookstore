import React, { useContext, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form';
import { AppContext } from '../../context/AppContext';
import { VALIDATIONS } from '../../constants/forms';
import Book from '../Book/Book';
import RouteGuard from '../RouteGuard';

export default function PurchaseForm() {

  const { books, handlePurchaseSubmit } = useContext(AppContext);

  const { bookID } = useParams();
  const history = useHistory();

  const { register, handleSubmit, reset, getValues, watch, formState: { errors, isSubmitSuccessful, isDirty } } = useForm({
    mode: 'all',
    defaultValues: {
      bookID,
      purchasedBy: {
        name: '',
        email: '',
        contact: ''
      }
    }
  })

  const [currentBook] = books.filter(book => book.id === +bookID);

  const [purchasePreview, setPurchasePreview] = useState(getValues())

  const formFields = [
    {
      name: 'purchasedBy.name',
      type: 'text',
      label: 'Name',
      registerParams: {
        required: VALIDATIONS.REQUIRED
      }
    },
    {
      name: 'purchasedBy.email',
      type: 'email',
      label: 'E-mail',
      registerParams: {
        required: VALIDATIONS.REQUIRED
      }
    },
    {
      name: 'purchasedBy.contact',
      type: 'text',
      label: 'Contact Number',
      registerParams: {
        required: VALIDATIONS.REQUIRED,
        pattern: {
          value: /^[0-9]{10}$/,
          message: 'Invalid contact number'
        }
      }
    }
  ];

  useEffect(() => {
    !isSubmitSuccessful && reset();
  }, [isSubmitSuccessful, reset]);

  useEffect(() => {
    const subscription = watch((value) => setPurchasePreview(value));
    return () => subscription.unsubscribe();
  }, [watch])



  return (
    <>
      <section className="p-5">
        <div className="flex items-center justify-between">
          <span className="font-bold text-2xl my-4 border-l-4 border-blue-600 px-2">Purchase Book</span>
        </div>
        <div className="flex flex-col md:flex-row items-center justify-center">
          <Book purchaselist
            book={{
              ...currentBook,
              purchaseDate: (new Date()).toDateString(),
              ...purchasePreview
            }} />
          <form className="rounded bg-gray-50 w-full max-w-md mt-10 md:mt-0 md:ml-5" onSubmit={handleSubmit(handlePurchaseSubmit)}>
            {
              !isSubmitSuccessful && (
                <div className="w-full flex flex-col p-6">
                  <div className="text-lg font-semibold">Customer Details</div>
                  <div className="text-sm text-gray-500 mt-1 mb-3">Please fill the below details to purchase the book</div>
                  <input type="number" name="bookID" {...register('bookID')} hidden />
                  {
                    formFields.map((field) => {

                      const error = errors?.purchasedBy && errors.purchasedBy[field.name.split('.')[1]];

                      return (
                        <div key={`${field.name}-${field.type}`} className="my-3 w-full">
                          <label htmlFor={field.name}>
                            <p className="font-semibold mb-2">
                              {field.label}
                              {field.registerParams?.required && <span className="text-red-600"> *</span>}
                            </p>
                          </label>
                          <input
                            id={field.name}
                            className="w-full border-2 border-gray-300 rounded p-2 focus:border-blue-600"
                            type={field.type}
                            {...register(field.name, field.registerParams)}
                          />
                          {
                            error &&
                            <p className="text-red-600 mt-1 text-sm tracking-wide">
                              {error.message}
                            </p>
                          }
                        </div>
                      )
                    })
                  }
                  <div className="flex items-center mt-4">
                    <input className="flex-grow px-4 py-2 bg-blue-600 hover:bg-blue-700 text-gray-50 rounded cursor-pointer" type="submit" value="Purchase" />
                  </div>
                </div>
              )
            }

            {
              isSubmitSuccessful && (
                <div className="w-full p-6 flex flex-col items-center justify-center relative text-gray-800">
                  <i className="material-icons-round border-2 p-1 border-green-300 rounded-full text-6xl text-green-500">check</i>
                  <p className="text-2xl text-center my-6">Congrats <b>{getValues().purchasedBy.name}</b>! 👏</p>
                  <p className="text-gray-600 p-2 text-center">You have successfully purchased the book <b>{currentBook.name}</b></p>
                  <button className="material-icons-round absolute top-0 right-0 text-3xl text-gray-300 pr-2 pt-2 hover:text-gray-400" onClick={() => {
                    reset();
                  }}>
                    clear
                  </button>
                  <button className="flex-grow mt-10 flex items-center justify-center px-4 py-2 bg-green-500 hover:bg-green-600 text-gray-50 rounded" onClick={() => {
                    history.push("/purchases");
                  }}>
                    Go to Purchases <i className="material-icons-round ml-5">arrow_right_alt</i>
                  </button>
                </div>
              )
            }
          </form>
        </div>
      </section>
      <RouteGuard when={isDirty && !isSubmitSuccessful} />
    </>
  )
}
